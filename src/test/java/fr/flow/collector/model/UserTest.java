/*
 * Copyright 2015 florian.grenot.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.flow.collector.model;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 * User Test Class.<br/>Test getter/setter and constraints validation.
 *
 * @author florian.grenot
 */
public class UserTest {

    private static final String FIRST_NAME = "Foo";
    private static final String LAST_NAME = "FOO";
    private static final String EMAIL = "FOO.Foo@mail.com";
    private static final String PASSWORD = "Passw0rd";

    private static Validator validator;

    /**
     * Set up Test Class to create validator for constraints validation.
     */
    @BeforeClass
    public static void setUpUserTestClass() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    /**
     * Test of getId method, of class User.
     */
    @Test
    public void testGetId() {
        User instance = new User();
        Integer result = instance.getId();
        Assert.assertNull(result);
    }

    /**
     * Test of setId method, of class User.
     */
    @Test
    public void testSetId() {
        Integer id = 10;
        User instance = new User();
        instance.setId(id);

        Integer expResult = 10;
        Integer result = instance.getId();
        Assert.assertEquals(expResult, result);

    }

    /**
     * Test of getLastName method, of class User.
     */
    @Test
    public void testGetLastName() {
        User instance = new User();
        String result = instance.getLastName();
        Assert.assertNull(result);
    }

    /**
     * Test of setLastName method, of class User.
     */
    @Test
    public void testSetLastName() {
        String lastName = LAST_NAME;
        User instance = new User();
        instance.setLastName(lastName);

        String expResult = LAST_NAME;
        String result = instance.getLastName();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of LastName constraints, of class User.
     */
    @Test
    public void testLastNameConstraints() {
        // Not Null constraint
        User user = new User(null, FIRST_NAME, EMAIL, PASSWORD, true);
        Set<ConstraintViolation<User>> constraintViolations = validator.validate(user);

        Assert.assertEquals(1, constraintViolations.size());

        String expResult = "{javax.validation.constraints.NotNull.message}";
        String result = constraintViolations.iterator().next().getMessageTemplate();
        Assert.assertEquals(expResult, result);

        // Not Empty constraint
        user.setLastName("");
        constraintViolations = validator.validate(user);

        Assert.assertEquals(1, constraintViolations.size());

        expResult = "{javax.validation.constraints.Size.message}";
        result = constraintViolations.iterator().next().getMessageTemplate();
        Assert.assertEquals(expResult, result);

        // Over 55 characters constraint
        user.setLastName("FOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO");
        constraintViolations = validator.validate(user);

        Assert.assertEquals(1, constraintViolations.size());

        expResult = "{javax.validation.constraints.Size.message}";
        result = constraintViolations.iterator().next().getMessageTemplate();
        Assert.assertEquals(expResult, result);

        // Good String
        user.setLastName(LAST_NAME);
        constraintViolations = validator.validate(user);

        Assert.assertEquals(0, constraintViolations.size());
    }

    /**
     * Test of getFirstName method, of class User.
     */
    @Test
    public void testGetFirstName() {
        User instance = new User();
        String result = instance.getFirstName();
        Assert.assertNull(result);
    }

    /**
     * Test of setFirstName method, of class User.
     */
    @Test
    public void testSetFirstName() {
        String firstName = FIRST_NAME;
        User instance = new User();
        instance.setFirstName(firstName);

        String expResult = FIRST_NAME;
        String result = instance.getFirstName();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of FirstName constraints, of class User.
     */
    @Test
    public void testFirstNameConstraints() {
        // Not Null constraint
        User user = new User(LAST_NAME, null, EMAIL, PASSWORD, true);
        Set<ConstraintViolation<User>> constraintViolations = validator.validate(user);

        Assert.assertEquals(1, constraintViolations.size());

        String expResult = "{javax.validation.constraints.NotNull.message}";
        String result = constraintViolations.iterator().next().getMessageTemplate();
        Assert.assertEquals(expResult, result);

        // Not Empty constraint
        user.setFirstName("");
        constraintViolations = validator.validate(user);

        Assert.assertEquals(1, constraintViolations.size());

        expResult = "{javax.validation.constraints.Size.message}";
        result = constraintViolations.iterator().next().getMessageTemplate();
        Assert.assertEquals(expResult, result);

        // Over 55 characters constraint
        user.setFirstName("Fooooooooooooooooooooooooooooooooooooooooooooooooooooooo");
        constraintViolations = validator.validate(user);

        Assert.assertEquals(1, constraintViolations.size());

        expResult = "{javax.validation.constraints.Size.message}";
        result = constraintViolations.iterator().next().getMessageTemplate();
        Assert.assertEquals(expResult, result);

        // Good String
        user.setFirstName(FIRST_NAME);
        constraintViolations = validator.validate(user);

        Assert.assertEquals(0, constraintViolations.size());
    }

    /**
     * Test of getEmail method, of class User.
     */
    @Test
    public void testGetEmail() {
        User instance = new User();
        String result = instance.getEmail();
        Assert.assertNull(result);
    }

    /**
     * Test of setEmail method, of class User.
     */
    @Test
    public void testSetEmail() {
        String email = EMAIL;
        User instance = new User();
        instance.setEmail(email);

        String expResult = EMAIL;
        String result = instance.getEmail();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of E-mail constraints, of class User.
     */
    @Test
    public void testEmailConstraints() {
        // Not Null constraint
        User user = new User(LAST_NAME, FIRST_NAME, null, PASSWORD, true);
        Set<ConstraintViolation<User>> constraintViolations = validator.validate(user);

        Assert.assertEquals(1, constraintViolations.size());

        String expResult = "{org.hibernate.validator.constraints.NotEmpty.message}";
        String result = constraintViolations.iterator().next().getMessageTemplate();
        Assert.assertEquals(expResult, result);

        // Not Empty constraint
        user.setEmail("");
        constraintViolations = validator.validate(user);

        Assert.assertEquals(1, constraintViolations.size());

        expResult = "{org.hibernate.validator.constraints.NotEmpty.message}";
        result = constraintViolations.iterator().next().getMessageTemplate();
        Assert.assertEquals(expResult, result);

        // E-mail regex constraint
        user.setEmail("NotAMail");
        constraintViolations = validator.validate(user);

        Assert.assertEquals(1, constraintViolations.size());

        expResult = "{org.hibernate.validator.constraints.Email.message}";
        result = constraintViolations.iterator().next().getMessageTemplate();
        Assert.assertEquals(expResult, result);

        // Good E-mail
        user.setEmail(EMAIL);
        constraintViolations = validator.validate(user);

        Assert.assertEquals(0, constraintViolations.size());
    }

    /**
     * Test of getPassword method, of class User.
     */
    @Test
    public void testGetPassword() {
        User instance = new User();
        String result = instance.getPassword();
        Assert.assertNull(result);
    }

    /**
     * Test of setPassword method, of class User.
     */
    @Test
    public void testSetPassword() {
        String password = PASSWORD;
        User instance = new User();
        instance.setPassword(password);

        String expResult = PASSWORD;
        String result = instance.getPassword();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of Password constraints, of class User.
     */
    @Test
    public void testPasswordConstraints() {
        // Not Null constraint
        User user = new User(LAST_NAME, FIRST_NAME, EMAIL, null, true);
        Set<ConstraintViolation<User>> constraintViolations = validator.validate(user);

        Assert.assertEquals(1, constraintViolations.size());

        String expResult = "{javax.validation.constraints.NotNull.message}";
        String result = constraintViolations.iterator().next().getMessageTemplate();
        Assert.assertEquals(expResult, result);

        // Not Empty constraint
        user.setPassword("");
        constraintViolations = validator.validate(user);

        Assert.assertEquals(1, constraintViolations.size());

        expResult = "{javax.validation.constraints.Size.message}";
        result = constraintViolations.iterator().next().getMessageTemplate();
        Assert.assertEquals(expResult, result);

        // Over 20 characters constraint
        user.setPassword("Passw0rdddddddddddddd");
        constraintViolations = validator.validate(user);

        Assert.assertEquals(1, constraintViolations.size());

        expResult = "{javax.validation.constraints.Size.message}";
        result = constraintViolations.iterator().next().getMessageTemplate();
        Assert.assertEquals(expResult, result);

        // Good password
        user.setPassword(PASSWORD);
        constraintViolations = validator.validate(user);

        Assert.assertEquals(0, constraintViolations.size());
    }

    /**
     * Test of getAvatar method, of class User.
     */
    @Test
    public void testGetAvatar() {
        User instance = new User();
        Image result = instance.getAvatar();
        Assert.assertNull(result);
    }

    /**
     * Test of setAvatar method, of class User.
     */
    @Test
    public void testSetAvatar() {
        Image avatar = new Image();
        User instance = new User();
        instance.setAvatar(avatar);

        Image expResult = new Image();
        Image result = instance.getAvatar();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of isAdmin method, of class User.
     */
    @Test
    public void testIsAdmin() {
        User instance = new User();
        boolean result = instance.isAdmin();
        Assert.assertFalse(result);
    }

    /**
     * Test of setAdmin method, of class User.
     */
    @Test
    public void testSetAdmin() {
        User instance = new User();
        instance.setAdmin(true);

        boolean result = instance.isAdmin();
        Assert.assertTrue(result);
    }

    /**
     * Test of getAvatars method, of class User.
     */
    @Test
    public void testGetAvatars() {
        User instance = new User();
        Set<Image> expResult = new HashSet<>(0);
        Set<Image> result = instance.getAvatars();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of setAvatars method, of class User.
     */
    @Test
    public void testSetAvatars() {
        Set<Image> avatars = new HashSet<>(0);
        User instance = new User();
        instance.setAvatars(avatars);

        Set<Image> expResult = new HashSet<>(0);
        Set<Image> result = instance.getAvatars();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getMoviesCollection method, of class User.
     */
    @Test
    public void testGetMoviesCollection() {
        User instance = new User();
        Set<Movie> expResult = new HashSet<>(0);
        Set<Movie> result = instance.getMoviesCollection();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of setMoviesCollection method, of class User.
     */
    @Test
    public void testSetMoviesCollection() {
        Set<Movie> moviesCollection = new HashSet<>(0);
        User instance = new User();
        instance.setMoviesCollection(moviesCollection);

        Set<Movie> expResult = new HashSet<>(0);
        Set<Movie> result = instance.getMoviesCollection();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of hashCode method, of class User.
     */
    @Test
    public void testHashCode() {
        User instance = new User();
        int expResult = 259; // 37 * 7 + 0
        int result = instance.hashCode();
        Assert.assertEquals(expResult, result);

        instance.setId(10);
        expResult = 269; // 37 * 7 + 10
        result = instance.hashCode();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class User.
     */
    @Test
    public void testEquals() {
        // Object null return false
        Object obj = null;
        User instance = new User();
        boolean result = instance.equals(obj);
        Assert.assertFalse(result);

        // Same object return true
        obj = instance;
        result = instance.equals(obj);
        Assert.assertTrue(result);

        // Differents objects but with same ID return true
        obj = new User();
        ((User) obj).setId(10);
        instance.setId(10);
        result = instance.equals(obj);
        Assert.assertTrue(result);
    }

    /**
     * Test of getAuthorities method, of class User.
     */
    @Test
    public void testGetAuthorities() {
        User instance = new User();
        Collection<? extends GrantedAuthority> expResult = Arrays.asList(new SimpleGrantedAuthority("ROLE_USER"));
        Collection<? extends GrantedAuthority> result = instance.getAuthorities();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of getUsername method, of class User.
     */
    @Test
    public void testGetUsername() {
        User instance = new User();
        String result = instance.getUsername();
        Assert.assertNull(result);
    }

    /**
     * Test of isAccountNonExpired method, of class User.
     */
    @Test
    public void testIsAccountNonExpired() {
        User instance = new User();
        boolean result = instance.isAccountNonExpired();
        Assert.assertTrue(result);
    }

    /**
     * Test of isAccountNonLocked method, of class User.
     */
    @Test
    public void testIsAccountNonLocked() {
        User instance = new User();
        boolean result = instance.isAccountNonLocked();
        Assert.assertTrue(result);
    }

    /**
     * Test of isCredentialsNonExpired method, of class User.
     */
    @Test
    public void testIsCredentialsNonExpired() {
        User instance = new User();
        boolean result = instance.isCredentialsNonExpired();
        Assert.assertTrue(result);
    }

    /**
     * Test of isEnabled method, of class User.
     */
    @Test
    public void testIsEnabled() {
        User instance = new User();
        boolean result = instance.isEnabled();
        Assert.assertTrue(result);
    }

}
