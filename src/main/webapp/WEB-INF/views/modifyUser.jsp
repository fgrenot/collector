<%--
    Document   : modifyUser
    Created on : 15 juin 2015, 11:28:32
    Author     : Florian
--%>

<%@ page contentType="text/html; charset=UTF-8" %>
<%@include file="include/header.jspf" %>

<div class="content">
    <div class="register">
        <div class="col-md-6 form-left">
            <h3><i18n:message code="modifyUser.title" text="missing" /></h3>
            <spr:form method="post" action="${pageContext.request.contextPath}/modify.htm" modelAttribute="user" id="modificationForm">
                <div>
                    <span><spr:label path="firstName"><i18n:message code="user.firstname" text="missing" />*</spr:label></span>
                    <spr:errors path="firstName" cssClass="alert-danger"/>
                    <spr:input path="firstName" cssClass="form-control"/>
                </div>
                <div>
                    <span><spr:label path="lastName"><i18n:message code="user.lastname" text="missing" />*</spr:label></span>
                    <spr:errors path="lastName" cssClass="alert-danger"/>
                    <spr:input path="lastName" cssClass="form-control"/>
                </div>
                <div>
                    <span><spr:label path="email"><i18n:message code="user.email" text="missing" />*</spr:label></span>
                    <spr:errors path="email" cssClass="alert-danger"/>
                    <spr:input type="email" path="email" cssClass="form-control"/>
                </div>
                <div class="news-letter">
                    <label class="checkbox">
                        <input type="checkbox" name="news-letter" checked="true" />
                        <i></i><i18n:message code="modifyUser.newsletter" text="missing" />
                    </label>
                </div>

                <h3><i18n:message code="register.login.info" text="missing" /></h3>
                <div>
                    <span><spr:label path="password"><i18n:message code="user.password" text="missing" />*</spr:label></span>
                    <spr:errors path="password" cssClass="alert-danger"/>
                    <spr:password path="password" cssClass="form-control"/>
                </div>

                <div>
                    <%-- FIXME: confirm doesn't work
                        <span><i18n:message code="user.password.confirm" text="missing" /><label>*</label></span>
                        <input type="password" class="form-control" name="confirmPassword" />
                    --%>
                </div>
                <div class="clearfix"></div>
                <div class="form-submit-grid">
                    <div class="form-button">
                        <input type="submit" value="<i18n:message code="util.modify" text="missing" />"/>
                        <div class="clearfix"> </div>
                    </div>
                </div>
            </spr:form>
        </div>
        <div class="col-md-6 form-right">
            <h3><i18n:message code="modifyUser.changeAvatar.title" text="missing" /></h3>
            <form method="post" action="${pageContext.request.contextPath}/image/uploadAvatar.htm" enctype="multipart/form-data">
                <div>
                    <span><label for="image">Change avatar</label></span>
                    <!--<div class="form-button">-->
                    <input type="file" name="image" accept="image/*"  class="form-button"/>
                    <!--</div>-->
                </div>
                <div>
                    <span><label for="name">Rename file</label></span>
                    <input type="text" name="name" placeholder="no change" class="form-control"/>
                </div>

                <div class="clearfix"> </div>
                <div class="form-button">
                    <input type="submit" value="Upload" />
                    <div class="clearfix"> </div>
                </div>
            </form>
        </div>

        <div class="clearfix"> </div>
    </div>
</div>

<%@include file="include/footer.jspf" %>