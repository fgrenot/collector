<%--
    Document   : viewMovies
    Created on : 24 juil. 2015, 11:21:23
    Author     : florian.grenot
--%>

<%@ page contentType="text/html; charset=UTF-8" %>
<%@include file="include/header.jspf" %>

<div class="content">
    <div class="movie_top">
        <div class="col-md-9 movie_box">
            <h2>${movie.title}</h2>
            <div class="grid images_3_of_2">
                <div class="movie_image">
                    <img src="${movie.posterURL}" class="img-responsive" alt=""/>
                </div>
                <div class="movie_rate">
                    <div class="rating_desc"><p>Your Vote :</p></div>
                    <form action="" class="sky-form">
                        <fieldset>
                            <section>
                                <div class="rating">
                                    <input type="radio" name="stars-rating" id="stars-rating-5">
                                    <label for="stars-rating-5"><i class="icon-star"></i></label>
                                    <input type="radio" name="stars-rating" id="stars-rating-4">
                                    <label for="stars-rating-4"><i class="icon-star"></i></label>
                                    <input type="radio" name="stars-rating" id="stars-rating-3">
                                    <label for="stars-rating-3"><i class="icon-star"></i></label>
                                    <input type="radio" name="stars-rating" id="stars-rating-2">
                                    <label for="stars-rating-2"><i class="icon-star"></i></label>
                                    <input type="radio" name="stars-rating" id="stars-rating-1">
                                    <label for="stars-rating-1"><i class="icon-star"></i></label>
                                </div>
                            </section>
                        </fieldset>
                    </form>
                    <div class="clearfix"> </div>
                </div>
            </div>
            <div class="desc1 span_3_of_2">
                <p class="movie_option"><strong>Country: </strong><a href="#">established</a>, <a href="#">USA</a></p>
                <p class="movie_option"><strong>Realase date: </strong>${movie.date}</p>
                <p class="movie_option"><strong>Category: </strong>
                    <c:forEach items="${movie.categories}" var="category">
                        <a href="#">${category.title}</a>,
                    </c:forEach>
                <p class="movie_option"><strong>Director: </strong><a href="#">suffered </a></p>
                <p class="movie_option"><strong>Actors: </strong>
                    <c:forEach items="${movie.actors}" var="actor">
                        <a href="#">${actor.firstName} ${actor.lastName}</a>,
                    </c:forEach>
                    <a href="#">...</a></p>
                <p class="movie_option"><strong>Age restriction: </strong>13</p>
                <div class="down_btn"><a class="btn1" href="#"><span> </span>Download</a></div>
            </div>
            <div class="clearfix"> </div>
            <p class="m_4">${movie.synopsis}</p>
        </div>
        <div class="col-md-3">
            <h3>In the same categories</h3>
            <div class="movie_img">
                <div class="grid_2 col_1">
                    <img src="${pageContext.request.contextPath}/resources/images/pic3.jpg" class="img-responsive" alt="">
                    <div class="caption1">
                        <p class="m_3">Iron Man</p>
                    </div>
                </div>
            </div>
            <div class="movie_img">
                <div class="grid_2 col_1">
                    <img src="${pageContext.request.contextPath}/resources/images/pic2.jpg" class="img-responsive" alt="">
                    <div class="caption1">
                        <p class="m_3">Captain America</p>
                    </div>
                </div>
            </div>
            <div class="movie_img">
                <div class="grid_2 col_1">
                    <img src="${pageContext.request.contextPath}/resources/images/pic9.jpg" class="img-responsive" alt="">
                    <div class="caption1">
                        <p class="m_3">The incredible Hulk</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>

<%@include file="include/footer.jspf" %>
