<%--
    Document   : movies
    Created on : 27 juil. 2015, 11:58:02
    Author     : florian.grenot
--%>

<%@ page contentType="text/html; charset=UTF-8" %>
<%@include file="include/header.jspf" %>

<div class="content">
    <div class="col-sm-3 col-md-4">
        <h2><i18n:message code="movies.title" text="missing" /></h2>
        <a href="?order=name_asc">sort by name ascending</a><br/>
        <a href="?order=name_desc">sort by name descending</a>
    </div>
    <div class="col-sm-offset-3 col-md-offset-5 col-sm-6 col-md-3">
        <form method="get" action="${pageContext.request.contextPath}/movie/searchMovie.htm" class="search-panel">
            <div class="form-group" style="display:inline;">
                <div class="input-group">
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-chevron-down"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="${pageContext.request.contextPath}/movie/searchMovie.htm"><i18n:message code="movies.searchMyMovies" text="missing" /></a></li>
                            <li><a href="${pageContext.request.contextPath}/movie/search.htm"><i18n:message code="movies.searchAllcineMovies" text="missing" /></a></li>
                        </ul>
                    </div>
                    <input type="text" name="q" class="form-control" placeholder="<i18n:message code="movies.searchMyMovies" text="missing" />">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                    </div>
                </div>
            </div>
        </form>
        <script>
            $(document).ready(function (e) {
                $('.search-panel .dropdown-menu').find('a').click(function (e) {
                    e.preventDefault();
                    var link = $(this).attr("href");
                    var value = $(this).text();
                    $('form').get(0).setAttribute('action', link);
                    $('input').get(0).setAttribute('placeholder', value);
                    $('.search-panel span#search_concept').text(value);
                    $('.input-group #search_param').val(link);
                });
            });
        </script>
    </div>
    <div class="movie_top">
        <div class="col-md-12">
            <c:forEach items="${movies}" var="movie" varStatus="status">
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <div class="grid_2 col_1">
                        <a href="${pageContext.request.contextPath}/movie/${movie.id}.htm">
                            <img src="${movie.posterURL}" class="img-responsive img-rounded" alt="">
                            <div class="caption1">
                                <p class="m_3">${movie.title}</p>
                            </div>
                        </a>
                    </div>
                </div>
                <c:if test="${status.index != 0 && status.index%2 == 1 && status.index%3 == 0}">
                    <div class="clearfix"> </div>
                </c:if>
            </c:forEach>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>

<%@include file="include/footer.jspf" %>
