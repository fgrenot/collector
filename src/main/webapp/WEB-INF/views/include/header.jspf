<%--
    A Design by W3layouts
    Design URL: http://w3layouts.com
    Design License: Creative Commons Attribution 3.0 Unported
    Design License URL: http://creativecommons.org/licenses/by/3.0/

    Author: Florian
--%>

<%@ page contentType="text/html; charset=UTF-8" %>

<%@taglib prefix="spr" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="i18n" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%-- Set User Principal as a variable for all pages --%>
<sec:authorize access="isAuthenticated()">
    <sec:authentication var="userPrincipal" property="principal" />
</sec:authorize>

<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Tester votre niveau">
        <meta name="author" content="fgrenot">

        <title><i18n:message code="page.title" text="missing" /></title>
        <link rel="shortcut icon" type="image/ico" href="${pageContext.request.contextPath}/resources/images/favicon.ico" />

        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

        <!-- Custom CSS -->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/collector.min.css">

        <!-- jQuery -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

        <script type="application/x-javascript">
            addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
            }, false);
            function hideURLbar() {
            window.scrollTo(0, 1);
            }
        </script>

        <!-- start plugins -->
        <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
        <script src="${pageContext.request.contextPath}/resources/js/responsiveslides.min.js"></script>
        <script>
            $(function () {
                $("#slider").responsiveSlides({
                    auto: true,
                    nav: true,
                    speed: 500,
                    namespace: "callbacks",
                    pager: true
                });
            });
        </script>
    </head>

    <body>
        <div class="container">
            <div class="container_wrap">
                <div class="header_top">
                    <div class="col-sm-3 logo">
                        <a href="${pageContext.request.contextPath}/">
                            <img src="${pageContext.request.contextPath}/resources/images/logo.png" alt=""/>
                        </a>
                    </div>
                    <div class="col-sm-6 nav">
                        <ul>
                            <li>
                                <span class="simptip-position-bottom simptip-movable" data-tooltip="comic">
                                    <a href="#"> </a>
                                </span>
                            </li>
                            <li>
                                <span class="simptip-position-bottom simptip-movable" data-tooltip="movie">
                                    <a href="${pageContext.request.contextPath}/movie/movies.htm"> </a>
                                </span>
                            </li>
                            <li>
                                <span class="simptip-position-bottom simptip-movable" data-tooltip="video">
                                    <a href="#"> </a>
                                </span>
                            </li>
                            <li>
                                <span class="simptip-position-bottom simptip-movable" data-tooltip="game">
                                    <a href="#"> </a>
                                </span>
                            </li>
                            <li>
                                <span class="simptip-position-bottom simptip-movable" data-tooltip="tv">
                                    <a href="#"> </a>
                                </span>
                            </li>
                            <li>
                                <span class="simptip-position-bottom simptip-movable" data-tooltip="more">
                                    <a href="#"> </a>
                                </span>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-3 header_right">
                        <ul class="header_right_box">
                            <sec:authorize access="!isAuthenticated()">

                                <li><p><a href="${pageContext.request.contextPath}/register.htm"><span class="glyphicon glyphicon-user"></span> <i18n:message code="user.register" text="missing" /></a></p></li>
                                <li><p><a href="${pageContext.request.contextPath}/login.htm"><i class="glyphicon glyphicon-log-in"></i> <i18n:message code="user.login" text="missing" /></a></p></li>

                            </sec:authorize>

                            <sec:authorize access="isAuthenticated()">

                                <li><img src="${pageContext.request.contextPath}/image/getUserAvatar.htm" alt="User Avatar" class="img-responsive img-circle"/></li>
                                <li class="dropdown pointer">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="">
                                        <span>
                                            <span class="hidden-tablet" translate="global.menu.account.main">
                                                <p>${userPrincipal.firstName} ${userPrincipal.lastName}</p>
                                            </span>
                                        </span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="${pageContext.request.contextPath}/modify.htm"><i class="glyphicon glyphicon-wrench"></i> <i18n:message code="user.settings" text="missing" /></a></li>
                                        <li><a href="${pageContext.request.contextPath}/j_spring_security_logout"><i class="glyphicon glyphicon-log-out"></i> <i18n:message code="user.logout" text="missing" /></a></li>
                                    </ul>
                                </li>

                            </sec:authorize>

                            <div class="clearfix"> </div>
                        </ul>
                    </div>
                    <div class="clearfix"> </div>
                </div>
