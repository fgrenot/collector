<%--
    Document   : login
    Created on : 15 juin 2015, 11:41:06
    Author     : Florian
--%>

<%@ page contentType="text/html; charset=UTF-8" %>
<%@include file="include/header.jspf" %>

<div class="content">
    <div class="register">
        <div class="col-md-6 form-left">
            <h3><i18n:message code="login.register.header" text="missing" /></h3>
            <p><i18n:message code="login.register.text" text="missing" /></p>
            <a class="acount-btn" href="${pageContext.request.contextPath}/register.htm"><i18n:message code="user.register" text="missing" /></a>
        </div>
        <div class="col-md-6 form-right">
            <h3><i18n:message code="login.registered.header" text="missing" /></h3>
            <p><i18n:message code="login.registered.text" text="missing" /></p>
            <spr:form method="post" action="j_spring_security_check" modelAttribute="user" name="loginForm">
                <div>
                    <span><spr:label path="email"><i18n:message code="user.email" text="missing" /></spr:label></span>
                    <spr:input path="email" title="E-mail" cssClass="form-control"/>
                </div>
                <div>
                    <span><spr:label path="password"><i18n:message code="user.password" text="missing" /></spr:label></span>
                    <spr:password path="password" title="Password" cssClass="form-control"/>
                </div>
                <a class="forgot" href="#"><i18n:message code="login.password.forgot" text="missing" /></a>

                <div class="form-button">
                    <input type="submit" value="<i18n:message code="user.login" text="missing" />"/>
                </div>
            </spr:form>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>

<%@include file="include/footer.jspf" %>