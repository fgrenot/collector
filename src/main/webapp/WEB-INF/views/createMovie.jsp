<%--
    Document   : createMovies
    Created on : 21 juil. 2015, 15:34:53
    Author     : florian.grenot
--%>

<%@ page contentType="text/html; charset=UTF-8" %>
<%@include file="include/header.jspf" %>

<div class="content">
    <div class="register">
        <spr:form method="post" action="${pageContext.request.contextPath}/movie/create.htm" modelAttribute="movie">
            <spr:hidden path="posterURL"/>
            <%--<spr:hidden path="actors"/>--%>
            <%--<spr:hidden path="categories"/>--%>
            <div class="col-md-6 form-left" >
                <h3>Create movie</h3>
                <div>
                    <span>Title<spr:label path="title">*</spr:label></span>
                    <spr:errors path="title" cssClass="alert-danger"/>
                    <spr:input path="title" cssClass="form-control"/>
                </div>

                <div>
                    <span>Duration<spr:label path="duration">*</spr:label></span>
                    <spr:errors path="duration" cssClass="alert-danger"/>
                    <spr:input path="duration" cssClass="form-control"/>
                </div>

                <div>
                    <span>date<spr:label path="date">*</spr:label></span>
                    <spr:errors path="date" cssClass="alert-danger"/>
                    <spr:input path="date" cssClass="form-control"/>
                </div>

                <div>
                    <span>Synopsis<spr:label path="synopsis">*</spr:label></span>
                    <spr:errors path="synopsis" cssClass="alert-danger"/>
                    <spr:textarea rows="8" path="synopsis" cssClass="form-control"/>
                </div>

                <h4>Actors:</h4>
                <div>
                    <ul>
                        <c:forEach items="${movie.actors}" var="actor" varStatus="status">

                            <li>${actor.firstName}<spr:hidden path="actors[${status.index}].firstName" value="${actor.firstName}"/></li>

                        </c:forEach>
                    </ul>
                </div>

                <h4>Categories:</h4>
                <div>
                    <ul>
                        <c:forEach items="${movie.categories}" var="category" varStatus="status">

                            <li>${category.title}<spr:hidden path="categories[${status.index}]" value="${category.title}"/></li>

                        </c:forEach>
                    </ul>
                </div>

            </div>

            <div class="col-md-6 form-right" >
                <div>
                    <label for="posterIMG">Poster du film</label>
                    <img src="${movie.posterURL}" class="img-responsive" id="posterIMG" />
                </div>
            </div>


            <div class="clearfix"> </div>
            <div class="form-submit-grid">
                <div class="form-button">
                    <input type="submit" value="create" />
                    <div class="clearfix"> </div>
                </div>
            </div>
        </spr:form>
    </div>
</div>

<%@include file="include/footer.jspf" %>
