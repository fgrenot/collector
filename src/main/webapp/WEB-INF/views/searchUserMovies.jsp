<%--
    Document   : searchUserMovies
    Created on : 28 juil. 2015, 15:34:53
    Author     : florian.grenot
--%>

<%@ page contentType="text/html; charset=UTF-8" %>
<%@include file="include/header.jspf" %>

<div class="content">
    <c:choose  >
        <c:when test="${fn:length(movies) == 0}" >
            <h2 class="m_3"><i18n:message code="search.noresult.title" text="missing" /></h2>
            <p><i18n:message code="search.noresult.text" text="missing" /></p>
        </c:when>
        <c:otherwise>
            <h2 class="m_3"><i18n:message code="searchMovie.title" text="missing" /></h2>
            <div class="movie_top">
                <div class="col-md-12">
                    <c:forEach items="${movies}" var="movie" varStatus="status">
                        <div class="col-md-3 ">
                            <div class="grid_2 col_1">
                                <a href="${pageContext.request.contextPath}/movie/${movie.id}.htm">
                                    <img src="${movie.posterURL}" class="img-responsive" alt="">
                                    <div class="caption1">
                                        <p class="m_3">${movie.title}</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <c:if test="${status.index != 0 && status.index%2 == 1 && status.index%3 == 0}">
                            <div class="clearfix"> </div>
                        </c:if>
                    </c:forEach>
                </div>
                <div class="clearfix"> </div>
            </div>
        </c:otherwise>
    </c:choose>
</div>

<%@include file="include/footer.jspf" %>
