/*
 * Copyright 2015 florian.grenot.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.flow.collector.service;

import fr.flow.collector.dao.AbstractDao;
import fr.flow.collector.dao.ActorDao;
import fr.flow.collector.model.Actor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author florian.grenot
 */
@Service
public class ActorService extends AbsrtactService<Actor> {

    private AbstractDao<Actor> dao;

    @Override
    public AbstractDao<Actor> getDao() {
        return dao;
    }

    @Autowired
    @Override
    public void setDao(AbstractDao<Actor> dao) {
        this.dao = dao;
        dao.setMetier(Actor.class);
    }

    public Actor search(String actorName) {
        return ((ActorDao) dao).searchActor(actorName);
    }

}
