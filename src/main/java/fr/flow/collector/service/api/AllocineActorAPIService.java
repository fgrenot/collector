/*
 * Copyright 2015 florian.grenot.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.flow.collector.service.api;

import com.moviejukebox.allocine.AllocineApi;
import com.moviejukebox.allocine.AllocineException;
import com.moviejukebox.allocine.model.PersonInfos;
import com.moviejukebox.allocine.model.Search;
import fr.flow.collector.converter.ActorConverter;
import fr.flow.collector.model.Actor;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author florian.grenot
 */
@Service
public class AllocineActorAPIService implements IAPIService<Actor> {

    private AllocineApi api;

    public AllocineApi getApi() {
        return api;
    }

    @Autowired
    public void setApi(AllocineApi api) {
        this.api = api;
    }

    @Override
    public Map<String, Actor> searchAll(String query) {
        if (null == query || query.isEmpty()) {
            throw new IllegalArgumentException();
        }

        Map<String, Actor> actors = null;
        try {
            Search result = api.searchPersons(query);
            if (null == result) {
                return null;
            }
            actors = ActorConverter.allocineActorListToActorMapConverter(result.getPersons());
        }
        catch (AllocineException ex) {
            Logger.getLogger(AllocineMovieAPIService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return actors;

    }

    @Override
    public Actor searchInfo(String id) {
        if (null == id || id.isEmpty()) {
            throw new IllegalArgumentException();
        }
        Actor actor = null;
        try {
            PersonInfos allocineActor = api.getPersonInfos(id);
            if (null == allocineActor) {
                return null;
            }
            actor = ActorConverter.allocinePersonInfosToActorConverter(allocineActor);
        }
        catch (AllocineException ex) {
            Logger.getLogger(AllocineActorAPIService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return actor;
    }

    public Actor searchByName(String name) {
        if (null == name || name.isEmpty()) {
            throw new IllegalArgumentException();
        }

        Map<String, Actor> actors = searchAll(name);
        if (null == actors) {
            return null;
        }

        Actor actor = null;
        for (String actorId : actors.keySet()) {
            if (null != actorId && !actorId.isEmpty()) {
                actor = searchInfo(actorId);
                break;
            }
        }
        return actor;
    }

}
