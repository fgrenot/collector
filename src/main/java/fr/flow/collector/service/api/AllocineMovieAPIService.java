/*
 * Copyright 2015 florian.grenot.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.flow.collector.service.api;

import com.moviejukebox.allocine.AllocineApi;
import com.moviejukebox.allocine.AllocineException;
import com.moviejukebox.allocine.model.MovieInfos;
import com.moviejukebox.allocine.model.Search;
import fr.flow.collector.converter.MovieConverter;
import fr.flow.collector.model.Movie;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author florian.grenot
 */
@Service
public class AllocineMovieAPIService implements IAPIService<Movie> {

    private AllocineApi api;

    public AllocineApi getApi() {
        return api;
    }

    @Autowired
    public void setApi(AllocineApi api) {
        this.api = api;
    }

    @Override
    public Map<String, Movie> searchAll(String query) {
        Map<String, Movie> movies = null;
        try {
            Search result = api.searchMovies(query);
            movies = MovieConverter.allocineMoviesListToMovieListConverter(result.getMovies());
        }
        catch (AllocineException ex) {
            Logger.getLogger(AllocineMovieAPIService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return movies;
    }

    @Override
    public Movie searchInfo(String id) {
        Movie movie = null;
        try {
            MovieInfos allocineMovie = api.getMovieInfos(id);
            movie = MovieConverter.allocineMovieInfosToMovieConverter(allocineMovie);
        }
        catch (AllocineException ex) {
            Logger.getLogger(AllocineActorAPIService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return movie;
    }

}
