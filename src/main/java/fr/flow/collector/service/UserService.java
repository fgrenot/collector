/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.flow.collector.service;

import fr.flow.collector.dao.AbstractDao;
import fr.flow.collector.dao.UserDao;
import fr.flow.collector.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 *
 * @author Florian
 */
@Service
public class UserService extends AbsrtactService<User> implements UserDetailsService {

    private AbstractDao<User> dao;

    @Override
    public AbstractDao<User> getDao() {
        return dao;
    }

    @Autowired
    @Override
    public void setDao(AbstractDao<User> dao) {
        this.dao = dao;
        dao.setMetier(User.class);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = ((UserDao) dao).selectByEmail(username);

        if (null == user) {
            throw new UsernameNotFoundException("The user: " + username + " doesn't exist.");
        }

        // Run text search index
        dao.index();

        return user;
    }

    public boolean isEmailExist(String email) throws UsernameNotFoundException {
        User user = ((UserDao) dao).selectByEmail(email);

        return null != user;
    }

}
