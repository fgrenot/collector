/*
 * Copyright 2015 florian.grenot.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.flow.collector.service;

import fr.flow.collector.dao.AbstractDao;
import fr.flow.collector.dao.ImageDao;
import fr.flow.collector.model.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;
import org.imgscalr.Scalr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author florian.grenot
 */
@Service
public class ImageService extends AbsrtactService<Image> {

    private AbstractDao<Image> dao;

    @Override
    public AbstractDao<Image> getDao() {
        return dao;
    }

    @Autowired
    @Override
    public void setDao(AbstractDao<Image> dao) {
        this.dao = dao;
        dao.setMetier(Image.class);
    }

    public Image readByName(String name) {
        return ((ImageDao) dao).selectByName(name);
    }

    public void transformToAvatar(Image profilImage) throws IOException {
        BufferedImage image = asBufferedImage(profilImage.getData());

        BufferedImage thumbnail = Scalr.resize(image, Scalr.Method.SPEED, 35);

        profilImage.setData(asByteArray(thumbnail, profilImage.getType()));
    }

    private BufferedImage asBufferedImage(byte[] profilImage) throws IOException {
        InputStream in = new ByteArrayInputStream(profilImage);
        return ImageIO.read(in);
    }

    private byte[] asByteArray(BufferedImage image, String type) throws IOException {
        byte[] imageInByte;

        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            ImageIO.write(image, type, baos);
            baos.flush();
            imageInByte = baos.toByteArray();
        }

        return imageInByte;
    }

}
