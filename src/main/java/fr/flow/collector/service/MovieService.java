/*
 * Copyright 2015 florian.grenot.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.flow.collector.service;

import fr.flow.collector.dao.AbstractDao;
import fr.flow.collector.dao.UserMovieDao;
import fr.flow.collector.model.Movie;
import fr.flow.collector.model.MovieActor;
import fr.flow.collector.model.MovieCategory;
import fr.flow.collector.model.User;
import fr.flow.collector.model.UserMovie;
import fr.flow.collector.model.UserMovieId;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author florian.grenot
 */
@Service
public class MovieService extends AbsrtactService<Movie> {

    private AbstractDao<Movie> movieDao;
    private AbstractDao<UserMovie> userMovieDao;
    private AbstractDao<MovieActor> movieActorDao;
    private AbstractDao<MovieCategory> movieCategoryDao;

    @Override
    public AbstractDao<Movie> getDao() {
        return movieDao;
    }

    @Autowired
    @Override
    public void setDao(AbstractDao<Movie> dao) {
        this.movieDao = dao;
        dao.setMetier(Movie.class);
    }

    public AbstractDao<UserMovie> getUserMovieDao() {
        return userMovieDao;
    }

    @Autowired
    public void setUserMovieDao(AbstractDao<UserMovie> userMovieDao) {
        this.userMovieDao = userMovieDao;
    }

    public AbstractDao<MovieActor> getMovieActorDao() {
        return movieActorDao;
    }

    @Autowired
    public void setMovieActorDao(AbstractDao<MovieActor> movieActorDao) {
        this.movieActorDao = movieActorDao;
    }

    public AbstractDao<MovieCategory> getMovieCategoryDao() {
        return movieCategoryDao;
    }

    @Autowired
    public void setMovieCategoryDao(AbstractDao<MovieCategory> movieCategoryDao) {
        this.movieCategoryDao = movieCategoryDao;
    }

    public void create(Movie movie, User user) {
        movieDao.insert(movie);

        createUserMovie(movie, user);

        user.getMoviesCollection().add(movie);
    }

    private void createUserMovie(Movie movie, User user) {
        UserMovieId id = new UserMovieId(user.getId(), movie.getId());
        UserMovie userMovie = new UserMovie(id, user, movie);
        userMovieDao.insert(userMovie);
    }

    public Movie readById(int id, User user) {
        for (Movie movie : user.getMoviesCollection()) {
            if (movie.getId() == id) {
                return movie;
            }
        }
        return null;
    }

    public List<Movie> searchAll(String query, User user) {
        return ((UserMovieDao) userMovieDao).searchMovies(query, user);
    }

}
