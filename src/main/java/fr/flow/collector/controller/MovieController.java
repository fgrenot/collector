/*
 * Copyright 2015 florian.grenot.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.flow.collector.controller;

import fr.flow.collector.model.Actor;
import fr.flow.collector.model.Category;
import fr.flow.collector.model.Movie;
import fr.flow.collector.model.User;
import fr.flow.collector.service.ActorService;
import fr.flow.collector.service.CategoryService;
import fr.flow.collector.service.MovieService;
import fr.flow.collector.service.api.AllocineActorAPIService;
import fr.flow.collector.service.api.AllocineMovieAPIService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author florian.grenot
 */
@Controller
@RequestMapping(value = "/movie")
public class MovieController {

    private AllocineMovieAPIService allocineMovieService;
    private AllocineActorAPIService allocineActorService;
    private MovieService movieService;
    private ActorService actorService;
    private CategoryService categoryService;

    public AllocineMovieAPIService getAllocineMovieService() {
        return allocineMovieService;
    }

    @Autowired
    public void setAllocineMovieService(AllocineMovieAPIService allocineMovieService) {
        this.allocineMovieService = allocineMovieService;
    }

    public AllocineActorAPIService getAllocineActorService() {
        return allocineActorService;
    }

    @Autowired
    public void setAllocineActorService(AllocineActorAPIService allocineActorService) {
        this.allocineActorService = allocineActorService;
    }

    public MovieService getmovieService() {
        return movieService;
    }

    @Autowired
    public void setMovieService(MovieService movieService) {
        this.movieService = movieService;
    }

    public ActorService getActorService() {
        return actorService;
    }

    @Autowired
    public void setActorService(ActorService actorService) {
        this.actorService = actorService;
    }

    public CategoryService getCategoryService() {
        return categoryService;
    }

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @RequestMapping(value = "/movies", method = RequestMethod.GET)
    public String movies(@RequestParam(required = false) String order, Model model) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Set<Movie> userMovies = user.getMoviesCollection();

        List<Movie> sortedMovies;

        switch (order == null ? "default" : order) {
            case "name_asc":
                sortedMovies = orderByNameAscending(userMovies);
                break;
            case "name_desc":
                sortedMovies = orderByNameDescending(userMovies);
                break;
            case "category":
                sortedMovies = orderByCategory(userMovies);
                break;
            default:
                sortedMovies = new ArrayList<>(userMovies);
        }

        model.addAttribute("movies", sortedMovies);

        return "movies";
    }

    private List<Movie> orderByNameAscending(Set<Movie> collection) {
        List<Movie> sortedMovies = new ArrayList<>(collection);
        Collections.sort(sortedMovies);
        return sortedMovies;
    }

    private List<Movie> orderByNameDescending(Set<Movie> collection) {
        List<Movie> sortedMovies = orderByNameAscending(collection);
        Collections.reverse(sortedMovies);
        return sortedMovies;
    }

    private List<Movie> orderByCategory(Set<Movie> collection) {
        throw new UnsupportedOperationException("Not yet implemented.");
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String searchName(@RequestParam(value = "q") String query, Model model) {
        Map<String, Movie> movies = allocineMovieService.searchAll(query);
        model.addAttribute("movies", movies);
        return "searchMovies";
    }

    @RequestMapping(value = "/searchMovie", method = RequestMethod.GET)
    public String searchPersonalMovie(@RequestParam(value = "q") String query, Model model) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        List<Movie> movies = movieService.searchAll(query, user);
        model.addAttribute("movies", movies);
        return "searchUserMovies";
    }

    @RequestMapping(value = "/searchAllocine", method = RequestMethod.GET)
    public String searchAllocineID(@RequestParam(value = "id") String id, Model model) {
        Movie movie = allocineMovieService.searchInfo(id);
        model.addAttribute("movie", movie);
        return "createMovie";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String createMovie(@Validated @ModelAttribute Movie movie, BindingResult result) {
        if (result.hasErrors()) {
            return "createMovie";
        }
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        checkActors(movie);
        checkCategories(movie);

        movieService.create(movie, user);

        return "redirect:/movie/" + movie.getId() + ".htm";
    }

    private void checkActors(Movie movie) {
        Set<Actor> actors = new HashSet<>(movie.getActors().size());
        for (Actor actor : movie.getActors()) {
            String actorName = actor.getFirstName();
            if (null != actorName && !actorName.isEmpty()) {
                Actor searchedActor = actorService.search(actorName);
                if (null == searchedActor) {
                    Actor allocineActor = allocineActorService.searchByName(actorName);
                    if (null != allocineActor) {
                        actorService.create(allocineActor);
                        actors.add(allocineActor);
                    }
                } else {
                    actors.add(searchedActor);
                }
            }
        }
        movie.setActors(new ArrayList<>(actors));
    }

    private void checkCategories(Movie movie) {
        Set<Category> categories = new HashSet<>(movie.getCategories().size());
        for (Category category : movie.getCategories()) {
            String title = category.getTitle();
            if (null != title && !title.isEmpty()) {
                Category searchedCategory = categoryService.search(title);
                if (null == searchedCategory) {
                    categoryService.create(category);
                    categories.add(category);
                } else {
                    categories.add(searchedCategory);
                }
            }
        }
        movie.setCategories(new ArrayList<>(categories));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String viewUserMovie(@PathVariable int id, Model model) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        model.addAttribute("movie", movieService.readById(id, user));

        return "viewMovie";
    }

}
