/*
 * Copyright 2015 florian.grenot.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.flow.collector.controller;

import fr.flow.collector.model.Image;
import fr.flow.collector.model.User;
import fr.flow.collector.security.CustomAuthenticationProvider;
import fr.flow.collector.service.UserService;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Florian
 */
@Controller
public class IndexController {

    private UserService userService;
    private CustomAuthenticationProvider authenticationProvider;

    public UserService getUserService() {
        return userService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public CustomAuthenticationProvider getAuthenticationProvider() {
        return authenticationProvider;
    }

    @Autowired
    public void setAuthenticationProvider(CustomAuthenticationProvider authenticationProvider) {
        this.authenticationProvider = authenticationProvider;
    }

    @RequestMapping(value = "/index")
    public String index() {
        return "index";
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String register(Model model) {
        model.addAttribute("user", new User());
        return "register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String register(@Validated User user, BindingResult result, HttpServletRequest request) {
        if (result.hasErrors()) {
            return "register";
        }
        if (userService.isEmailExist(user.getEmail())) {
            // FIXME The error has not been show on the page
            result.addError(new ObjectError("email", "validation.mail.unique"));
            return "register";
        }

        // add default avatar. need avatar_id not null...
        Image avatar = new Image();
        avatar.setId(1);
        user.setAvatar(avatar);

        userService.create(user);

        doAutoLogin(user.getEmail(), user.getPassword(), request);
        return "index";
    }

    private void doAutoLogin(String username, String password, HttpServletRequest request) {
        try {
            // Must be called from request filtered by Spring Security, otherwise SecurityContextHolder is not updated
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
            token.setDetails(new WebAuthenticationDetails(request));
            Authentication authentication = this.authenticationProvider.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }
        catch (Exception ex) {
            Logger.getLogger(IndexController.class.getName()).log(Level.SEVERE, null, ex);

            SecurityContextHolder.getContext().setAuthentication(null);
        }

    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model) {
        model.addAttribute("user", new User());
        return "login";
    }

    @RequestMapping(value = "/modify", method = RequestMethod.GET)
    public String modify(Model model) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("user", user);
        return "modifyUser";
    }

    @RequestMapping(value = "/modify", method = RequestMethod.POST)
    public String modify(@Validated User user, BindingResult result) {
        if (result.hasErrors()) {
            return "modifyUser";
        }

        User principal = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        principal.setFirstName(user.getFirstName());
        principal.setLastName(user.getLastName());
        principal.setEmail(user.getEmail());
        principal.setPassword(user.getPassword());

        userService.update(principal);

        return "redirect:/";
    }

}
