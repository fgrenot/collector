/*
 * Copyright 2015 florian.grenot.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.flow.collector.controller;

import fr.flow.collector.model.User;
import fr.flow.collector.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Florian
 */
@Controller
@RequestMapping(value = "/user")
public class UserController {

    private UserService userService;

    public UserService getUserService() {
        return userService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String add(Model model) {
        model.addAttribute("user", new User());
        return "addUser";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String add(@Validated User user, BindingResult result) {
        if (result.hasErrors()) {
            return "addUser";
        }

        userService.create(user);

        return "redirect:/user/view.htm";
    }

    @RequestMapping(value = "/view")
    public String view(Model model) {
        model.addAttribute("users", userService.readAll());
        return "viewUsers";
    }

    @RequestMapping(value = "/modify", method = RequestMethod.GET)
    public String modify(@RequestParam(value = "id") int id, Model model) {
        model.addAttribute("user", userService.readById(id));
        return "modifyUser";
    }

    @RequestMapping(value = "/modify", method = RequestMethod.PUT)
    public String modify(@Validated User user, BindingResult result) {
        if (result.hasErrors()) {
            return "modifyUser";
        }

        userService.update(user);

        return "redirect:/user/view.htm";
    }

    @RequestMapping(value = "/remove")
    public String remove(@RequestParam(value = "id") int id) {
        User user = userService.readById(id);
        userService.delete(user);

        return "redirect:/user/view.htm";
    }

}
