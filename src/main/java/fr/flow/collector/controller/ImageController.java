/*
 * Copyright 2015 florian.grenot.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.flow.collector.controller;

import fr.flow.collector.model.Image;
import fr.flow.collector.model.User;
import fr.flow.collector.service.ImageService;
import fr.flow.collector.service.UserService;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author florian.grenot
 */
@Controller
@RequestMapping(value = "/image")
public class ImageController {

    private ImageService imageService;
    private UserService userService;

    public ImageService getImageService() {
        return imageService;
    }

    @Autowired
    public void setImageService(ImageService imageService) {
        this.imageService = imageService;
    }

    public UserService getUserService() {
        return userService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    /**
     * To use :
     * <img src="${pageContext.request.contextPath}/image/getUserAvatar.htm"/>
     *
     * @return
     * @throws java.io.IOException
     */
    @RequestMapping(value = "/getUserAvatar", method = RequestMethod.GET)
    @ResponseBody
    public byte[] userAvatar() throws IOException {
        User auth = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Image image = auth.getAvatar();
        if (image.getData().length == image.getSize()) {
            imageService.transformToAvatar(image);
        }
        return image.getData();
    }

    @RequestMapping(value = "/uploadAvatar", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void uploadFileHandler(@RequestParam("name") String name, @RequestParam("image") MultipartFile file) throws IOException {
        if (file.isEmpty()) {
            // TODO: upload failed, set a return to the user.
            return;
        }
        Image avatar = new Image("".equals(name) ? file.getOriginalFilename().split("\\.")[0] : name,
                file.getContentType().split("/")[1],
                (int) file.getSize(),
                file.getBytes());

        imageService.create(avatar);
        User auth = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        auth.setAvatar(avatar);

        userService.update(auth);
    }

    /**
     * To use :
     * <img src="${pageContext.request.contextPath}/image/search.htm?q=admin%20avatar" alt="User Avatar"/>
     *
     * @param name
     * @return
     */
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    @ResponseBody
    public byte[] imageByName(@RequestParam(value = "q") String name) {
        Image image = imageService.readByName(name);

        return image.getData();
    }

}
