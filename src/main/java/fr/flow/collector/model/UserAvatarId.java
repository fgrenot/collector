/*
 * Copyright 2015 florian.grenot.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.flow.collector.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author florian.grenot
 */
@Embeddable
public class UserAvatarId implements java.io.Serializable {

    private int userId;
    private int avatarId;

    public UserAvatarId() {
    }

    public UserAvatarId(int userId, int avatarId) {
        this.userId = userId;
        this.avatarId = avatarId;
    }

    @Column(name = "user_id", nullable = false)
    public int getUserId() {
        return this.userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Column(name = "avatar_id", nullable = false)
    public int getAvatarId() {
        return this.avatarId;
    }

    public void setAvatarId(int avatarId) {
        this.avatarId = avatarId;
    }

    @Override
    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if ((other == null)) {
            return false;
        }
        if (!(other instanceof UserAvatarId)) {
            return false;
        }
        UserAvatarId castOther = (UserAvatarId) other;

        return (this.getUserId() == castOther.getUserId())
                && (this.getAvatarId() == castOther.getAvatarId());
    }

    @Override
    public int hashCode() {
        int result = 17;

        result = 37 * result + this.getUserId();
        result = 37 * result + this.getAvatarId();
        return result;
    }

}
