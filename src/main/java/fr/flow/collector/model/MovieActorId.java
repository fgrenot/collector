/*
 * Copyright 2015 florian.grenot.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.flow.collector.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author florian.grenot
 */
@Embeddable
public class MovieActorId implements java.io.Serializable {

    private int movieId;
    private int actorId;

    public MovieActorId() {
    }

    public MovieActorId(int movieId, int actorId) {
        this.movieId = movieId;
        this.actorId = actorId;
    }

    @Column(name = "movie_id", nullable = false)
    public int getMovieId() {
        return this.movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

    @Column(name = "actor_id", nullable = false)
    public int getActorId() {
        return this.actorId;
    }

    public void setActorId(int actorId) {
        this.actorId = actorId;
    }

    @Override
    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if ((other == null)) {
            return false;
        }
        if (!(other instanceof MovieActorId)) {
            return false;
        }
        MovieActorId castOther = (MovieActorId) other;

        return (this.getMovieId() == castOther.getMovieId())
                && (this.getActorId() == castOther.getActorId());
    }

    @Override
    public int hashCode() {
        int result = 17;

        result = 37 * result + this.getMovieId();
        result = 37 * result + this.getActorId();
        return result;
    }

}
