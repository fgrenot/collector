/*
 * Copyright 2015 florian.grenot.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.flow.collector.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Type;
import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Store;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author florian.grenot
 */
@Entity
@Indexed
@Table(name = "movie", catalog = "collector")
public class Movie extends Business implements Serializable, Comparable<Movie> {

    private Integer id;

    @NotNull
    @Length(min = 1, max = 255)
    @Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
    private String title;

    @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
    private LocalTime duration;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate date;

    @Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
    private String synopsis;
    private String posterURL;
    private List<Category> categories = new ArrayList<>(0);
    private List<Actor> actors = new ArrayList<>(0);

    public Movie() {
    }

    public Movie(String title, LocalTime duration, LocalDate date, String synopsis) {
        this.title = title;
        this.duration = duration;
        this.date = date;
        this.synopsis = synopsis;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "title", nullable = false)
    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Type(type = "fr.flow.collector.converter.hibernateUtils.LocalTimeUserType")
    @Column(name = "duration", nullable = false, length = 8)
    public LocalTime getDuration() {
        return this.duration;
    }

    public void setDuration(LocalTime duration) {
        this.duration = duration;
    }

    @Type(type = "fr.flow.collector.converter.hibernateUtils.LocalDateUserType")
    @Column(name = "date", nullable = false)
    public LocalDate getDate() {
        return this.date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Column(name = "synopsis", nullable = false, length = 65535)
    public String getSynopsis() {
        return this.synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    @Column(name = "poster_url", nullable = true, length = 255)
    public String getPosterURL() {
        return this.posterURL;
    }

    public void setPosterURL(String posterURL) {
        this.posterURL = posterURL;
    }

    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "movie_category", catalog = "collector", joinColumns = {
        @JoinColumn(name = "movie_id", nullable = false, updatable = false)}, inverseJoinColumns = {
        @JoinColumn(name = "category_id", nullable = false, updatable = false)})
    public List<Category> getCategories() {
        return this.categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "movie_actor", catalog = "collector", joinColumns = {
        @JoinColumn(name = "movie_id", nullable = false, updatable = false)}, inverseJoinColumns = {
        @JoinColumn(name = "actor_id", nullable = false, updatable = false)})
    public List<Actor> getActors() {
        return this.actors;
    }

    public void setActors(List<Actor> actors) {
        this.actors = actors;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Movie other = (Movie) obj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public int compareTo(Movie o) {
        return this.title.compareToIgnoreCase(o.getTitle());
    }

}
