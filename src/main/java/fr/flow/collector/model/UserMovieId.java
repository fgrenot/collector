/*
 * Copyright 2015 florian.grenot.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.flow.collector.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author florian.grenot
 */
@Embeddable
public class UserMovieId implements java.io.Serializable {

    private int userId;
    private int movieId;

    public UserMovieId() {
    }

    public UserMovieId(int userId, int movieId) {
        this.userId = userId;
        this.movieId = movieId;
    }

    @Column(name = "user_id", nullable = false)
    public int getUserId() {
        return this.userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Column(name = "id_answer", nullable = false)
    public int getMovieId() {
        return this.movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + this.userId;
        hash = 47 * hash + this.movieId;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (null == obj) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserMovieId other = (UserMovieId) obj;
        if (this.userId != other.userId) {
            return false;
        }
        return this.movieId == other.movieId;
    }

}
