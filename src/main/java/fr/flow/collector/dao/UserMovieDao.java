/*
 * Copyright 2015 florian.grenot.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.flow.collector.dao;

import fr.flow.collector.model.Movie;
import fr.flow.collector.model.User;
import fr.flow.collector.model.UserMovie;
import fr.flow.collector.util.Lists;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.stereotype.Repository;

/**
 *
 * @author florian.grenot
 */
@Repository
public class UserMovieDao extends AbstractDao<UserMovie> {

    public Movie selectByIds(int movieId, int userId) {
        UserMovie userMovie = null;

        Session session = null;
        try {
            session = getSession();

            Query query = session.createQuery("FROM UserMovie WHERE user_id = :userId AND movie_id = :movieId");
            query.setInteger("userId", userId);
            query.setInteger("movieId", movieId);

            userMovie = (UserMovie) query.uniqueResult();
        }
        catch (HibernateException ex) {
            Logger.getLogger(UserMovieDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return userMovie == null ? null : userMovie.getMovie();
    }

    @SuppressWarnings("unchecked")
    public List<Movie> searchMovies(String movieTitle, User user) {
        List<Movie> movies = new ArrayList<>(0);

        FullTextSession fullTextSession = null;
        Transaction transaction = null;
        try {
            fullTextSession = Search.getFullTextSession(getSession());
            transaction = fullTextSession.beginTransaction();

            QueryBuilder queryBuilder = fullTextSession.getSearchFactory()
                    .buildQueryBuilder().forEntity(Movie.class).get();

            org.apache.lucene.search.Query apacheQuery = queryBuilder
                    .keyword().onFields("title", "synopsis")
                    .matching(movieTitle)
                    .createQuery();

            Query hibernateQuery = fullTextSession.createFullTextQuery(apacheQuery, Movie.class);

            List<Movie> queryMovies = hibernateQuery.list();
            List<Movie> userMovies = new ArrayList<>(user.getMoviesCollection());

            movies = Lists.intersection(queryMovies, userMovies);

            transaction.commit();
        }
        catch (HibernateException ex) {
            Logger.getLogger(UserMovieDao.class.getName()).log(Level.SEVERE, null, ex);
            if (null != transaction) {
                transaction.rollback();
            }
        }
        finally {
            if (fullTextSession != null && fullTextSession.isOpen()) {
                fullTextSession.close();
            }
        }

        return movies;
    }

}
