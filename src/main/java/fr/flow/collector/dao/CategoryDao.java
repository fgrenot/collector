/*
 * Copyright 2015 florian.grenot.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.flow.collector.dao;

import fr.flow.collector.model.Category;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

/**
 *
 * @author florian.grenot
 */
@Repository
public class CategoryDao extends AbstractDao<Category> {

    public Category searchCategory(String categoryTitle) {
        Category category = null;

        Session session = null;
        try {
            session = getSession();

            Query query = session.createQuery("FROM Category WHERE title = :title");
            query.setString("title", categoryTitle);

            category = (Category) query.uniqueResult();
        }
        catch (HibernateException ex) {
            Logger.getLogger(CategoryDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return category;
    }
}
