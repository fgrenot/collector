/*
 * Copyright 2015 florian.grenot.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.flow.collector.dao;

import fr.flow.collector.model.Actor;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Transaction;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.stereotype.Repository;

/**
 *
 * @author florian.grenot
 */
@Repository
public class ActorDao extends AbstractDao<Actor> {

    public Actor searchActor(String actorName) {
        Actor actor = null;

        FullTextSession fullTextSession = null;
        Transaction transaction = null;
        try {
            fullTextSession = Search.getFullTextSession(getSession());
            transaction = fullTextSession.beginTransaction();

            QueryBuilder queryBuilder = fullTextSession.getSearchFactory()
                    .buildQueryBuilder().forEntity(Actor.class).get();

            String[] decomposedName = actorName.split(" ", 2);
            org.apache.lucene.search.Query apacheQuery = queryBuilder.bool()
                    .must(queryBuilder.keyword()
                            .onFields("firstName")
                            .matching(decomposedName[0])
                            .createQuery())
                    .must(queryBuilder.keyword()
                            .onFields("lastName")
                            .matching(decomposedName[1])
                            .createQuery())
                    .createQuery();

            Query hibernateQuery = fullTextSession.createFullTextQuery(apacheQuery, Actor.class);
            hibernateQuery.setMaxResults(1);

            actor = (Actor) hibernateQuery.uniqueResult();

            transaction.commit();
        }
        catch (HibernateException ex) {
            Logger.getLogger(ActorDao.class.getName()).log(Level.SEVERE, null, ex);
            if (null != transaction) {
                transaction.rollback();
            }
        }
        finally {
            if (fullTextSession != null && fullTextSession.isOpen()) {
                fullTextSession.close();
            }
        }

        return actor;
    }

}
