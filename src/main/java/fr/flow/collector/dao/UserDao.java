/*
 * Copyright 2015 florian.grenot.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.flow.collector.dao;

import fr.flow.collector.model.User;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Florian
 */
@Repository
public class UserDao extends AbstractDao<User> {

    public User selectByEmail(String email) {
        User user = null;

        Session session = null;
        try {
            session = getSession();

            Query query = session.createQuery("FROM User WHERE email = :email");
            query.setString("email", email);

            user = (User) query.uniqueResult();
        }
        catch (HibernateException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return user;
    }

}
