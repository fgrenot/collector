/*
 * Copyright 2015 florian.grenot.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.flow.collector.dao;

import fr.flow.collector.model.Business;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.CacheMode;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Florian
 * @param <T>
 */
public abstract class AbstractDao<T extends Business> implements IDao<T> {

    private SessionFactory sessionFactory;

    private Class<T> clazz;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    protected Session getSession() {
        return sessionFactory.openSession();
    }

    public Class<T> getMetier() {
        return clazz;
    }

    public void setMetier(Class<T> metier) {
        this.clazz = metier;
    }

    public void index() {
        FullTextSession fullTextSession = null;
        Transaction transaction = null;
        try {
            fullTextSession = Search.getFullTextSession(getSession());
            transaction = fullTextSession.beginTransaction();

            fullTextSession
                    .createIndexer()
                    .batchSizeToLoadObjects(50)
                    .cacheMode(CacheMode.NORMAL)
                    .threadsToLoadObjects(6)
                    .startAndWait();

            transaction.commit();
        }
        catch (HibernateException | InterruptedException ex) {
            Logger.getLogger(AbstractDao.class.getName()).log(Level.SEVERE, null, ex);
            if (null != transaction) {
                transaction.rollback();
            }
        }
        finally {
            if (null != fullTextSession && fullTextSession.isOpen()) {
                fullTextSession.close();
            }
        }
    }

    @Override
    public void insert(T metier) {
        Session session = null;
        try {
            session = getSession();
            session.beginTransaction();

            session.save(metier);

            session.getTransaction().commit();
        }
        catch (HibernateException ex) {
            Logger.getLogger(AbstractDao.class.getName()).log(Level.SEVERE, null, ex);
            if (session != null) {
                session.getTransaction().rollback();
            }
        }
        finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public void update(T metier) {
        Session session = null;
        try {
            session = getSession();
            session.beginTransaction();

            session.update(metier);

            session.getTransaction().commit();
        }
        catch (HibernateException ex) {
            Logger.getLogger(AbstractDao.class.getName()).log(Level.SEVERE, null, ex);
            if (session != null) {
                session.getTransaction().rollback();
            }
        }
        finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public void delete(T metier) {
        Session session = null;
        try {
            session = getSession();
            session.beginTransaction();

            session.delete(metier);

            session.getTransaction().commit();
        }
        catch (HibernateException ex) {
            Logger.getLogger(AbstractDao.class.getName()).log(Level.SEVERE, null, ex);
            if (session != null) {
                session.getTransaction().rollback();
            }
        }
        finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<T> selectAll() {
        List<T> metiers = null;

        Session session = null;
        try {
            session = getSession();

            Query query = session.createQuery("FROM " + clazz.getSimpleName());
            metiers = query.list();
        }
        catch (HibernateException ex) {
            Logger.getLogger(AbstractDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return metiers;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T selectById(int id) {
        T metier = null;

        Session session = null;
        try {
            session = getSession();

            metier = (T) session.get(clazz, id);
        }
        catch (HibernateException ex) {
            Logger.getLogger(AbstractDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return metier;
    }

}
