/*
 * Copyright 2015 florian.grenot.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.flow.collector.dao;

import fr.flow.collector.model.Business;
import java.util.List;

/**
 *
 * @author Florian
 * @param <T>
 */
public interface IDao<T extends Business> {

    public void insert(T objet);

    public void update(T objet);

    public void delete(T objet);

    public List<T> selectAll();

    public T selectById(int id);

}
