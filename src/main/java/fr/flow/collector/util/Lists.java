/*
 * Copyright 2015 florian.grenot.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.flow.collector.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author florian.grenot
 */
public class Lists {

    public static <T> List<T> union(List<T> list1, List<T> list2) {
        Objects.requireNonNull(list1);
        Objects.requireNonNull(list2);

        Set<T> set = new HashSet<>();

        set.addAll(list1);
        set.addAll(list2);

        return new ArrayList<>(set);
    }

    public static <T> List<T> intersection(List<T> list1, List<T> list2) {
        Objects.requireNonNull(list1);
        Objects.requireNonNull(list2);

        List<T> list = new ArrayList<>();

        for (T t : list1) {
            if (list2.contains(t)) {
                list.add(t);
            }
        }

        return list;
    }

    public static <T> List<T> asSet(List<T> list) {
        Objects.requireNonNull(list);

        if (null == list) {
            return null;
        }
        List<T> result = new ArrayList<>(new HashSet<>(list));
        return result;
    }

    private Lists() {
    }

}
