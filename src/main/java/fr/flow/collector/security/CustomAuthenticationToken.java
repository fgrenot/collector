/*
 * Copyright 2015 florian.grenot.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.flow.collector.security;

import fr.flow.collector.model.User;
import java.util.Collection;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author Florian
 */
public class CustomAuthenticationToken extends AbstractAuthenticationToken {

    private final Object principal;

    Collection<GrantedAuthority> authorities;

    @SuppressWarnings("unchecked")
    public CustomAuthenticationToken(User user) {
        super(user.getAuthorities());
        super.setAuthenticated(true);

        this.authorities = (Collection<GrantedAuthority>) user.getAuthorities();
        this.principal = user;
    }

    @Override
    public Object getCredentials() {
        return "";
    }

    @Override
    public Object getPrincipal() {
        return principal;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return authorities;
    }

}
