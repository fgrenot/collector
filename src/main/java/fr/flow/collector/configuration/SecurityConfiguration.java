/*
 * Copyright 2015 florian.grenot.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.flow.collector.configuration;

import fr.flow.collector.security.CustomAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.filter.RequestContextFilter;

/**
 *
 * @author Florian
 */
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    UserDetailsService userDetailsService;

    @Autowired
    CustomAuthenticationProvider authentificationProvider;

    public UserDetailsService getUserDetailsService() {
        return userDetailsService;
    }

    public void setUserDetailsService(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    public CustomAuthenticationProvider getAuthetificationProvider() {
        return authentificationProvider;
    }

    public void setAuthetificationProvider(CustomAuthenticationProvider authetificationProvider) {
        this.authentificationProvider = authetificationProvider;
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authentificationProvider).userDetailsService(userDetailsService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authenticationProvider(authentificationProvider);

        http.authorizeRequests().antMatchers("/resources/**").permitAll();
        http.authorizeRequests().antMatchers("/index*").permitAll();
        http.authorizeRequests().antMatchers("/register*").permitAll();
        http.authorizeRequests().antMatchers("/login*").permitAll();
        http.authorizeRequests().antMatchers("/j_spring_security_checks").permitAll();

        http.authorizeRequests().antMatchers("/user/**").access("hasRole('ROLE_ADMIN')");
        http.authorizeRequests().antMatchers("/**").fullyAuthenticated();

        http.formLogin()
                .loginPage("/login.htm")
                .loginProcessingUrl("/j_spring_security_check")
                .failureUrl("/login.htm")
                .usernameParameter("email")
                .passwordParameter("password");

        http.logout()
                .logoutUrl("/j_spring_security_logout")
                .logoutSuccessUrl("/")
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID");

        http.addFilterBefore(new RequestContextFilter(), UsernamePasswordAuthenticationFilter.class);
        http.csrf().disable();
    }

}
