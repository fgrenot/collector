/*
 * Copyright 2015 florian.grenot.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.flow.collector.configuration;

import java.util.Properties;
import javax.sql.DataSource;
import org.hibernate.SessionFactory;
import org.jasypt.encryption.pbe.config.EnvironmentStringPBEConfig;
import org.jasypt.hibernate4.encryptor.HibernatePBEStringEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *
 * @author Florian
 */
@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = "fr.flow.collector.configuration")
@PropertySource(value = "classpath:application.properties")
public class HibernateConfiguration {

    private Environment environment;

    public Environment getEnvironment() {
        return environment;
    }

    @Autowired
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan(new String[]{"fr.flow.collector.model", "fr.flow.collector.converter.hibernateUtils"});
        sessionFactory.setAnnotatedPackages(new String[]{"fr.flow.collector.converter.hibernateUtils"});
        sessionFactory.setHibernateProperties(hibernateProperties());
        return sessionFactory;
    }

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(environment.getRequiredProperty("jdbc.driverClassName"));
        dataSource.setUrl(environment.getRequiredProperty("jdbc.url"));
        dataSource.setUsername(environment.getRequiredProperty("jdbc.username"));
        dataSource.setPassword(environment.getRequiredProperty("jdbc.password"));
        return dataSource;
    }

    private Properties hibernateProperties() {
        Properties properties = new Properties();
        properties.put("hibernate.dialect", environment.getRequiredProperty("hibernate.dialect"));
        properties.put("hibernate.show_sql", environment.getRequiredProperty("hibernate.show_sql"));
        properties.put("hibernate.format_sql", environment.getRequiredProperty("hibernate.format_sql"));
        properties.put("hibernate.cache.provider_class", environment.getRequiredProperty("hibernate.cache.provider_class"));

        properties.put("hibernate.search.lucene_version", environment.getRequiredProperty("hibernate.search.lucene_version"));
        properties.put("hibernate.search.default.directory_provider", environment.getRequiredProperty("hibernate.search.default.directory_provider"));
        properties.put("hibernate.search.default.indexBase", System.getProperty("java.io.tmpdir") + System.getProperty("file.separator") + environment.getRequiredProperty("hibernate.search.default.indexBase"));
        properties.put("hibernate.search.default.locking_strategy", environment.getRequiredProperty("hibernate.search.default.locking_strategy"));
        properties.put("hibernate.search.default.exclusive_index_use", environment.getRequiredProperty("hibernate.search.default.exclusive_index_use"));
        properties.put("hibernate.search.worker.thread_pool.size", environment.getRequiredProperty("hibernate.search.worker.thread_pool.size"));
        properties.put("hibernate.search.worker.batch_size", environment.getRequiredProperty("hibernate.search.worker.batch_size"));

        return properties;
    }

    @Bean
    @Autowired
    public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
        HibernateTransactionManager transactionManager = new HibernateTransactionManager();
        transactionManager.setSessionFactory(sessionFactory);
        return transactionManager;
    }

    @Bean
    public EnvironmentStringPBEConfig stringPBEConfig() {
        EnvironmentStringPBEConfig config = new EnvironmentStringPBEConfig();
        config.setAlgorithm("PBEWithMD5AndDES");
//        config.setPasswordEnvName("APP_ENCRYPTION_PASSWORD");
        config.setPassword("fitec"); // FIXME utiliser ce qu'il y a au dessus !
        return config;
    }

    @Bean
    public HibernatePBEStringEncryptor hibernatePBEStringEncryptor() {
        HibernatePBEStringEncryptor encryptor = new HibernatePBEStringEncryptor();
        encryptor.setConfig(stringPBEConfig());
        encryptor.setRegisteredName("hibernateStringEncryptor");
        return encryptor;
    }

}
