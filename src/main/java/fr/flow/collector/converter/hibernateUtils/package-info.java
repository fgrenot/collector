/*
 * Copyright 2015 florian.grenot.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
@TypeDefs({
    @TypeDef(name = "localDateType",
            defaultForType = LocalDate.class,
            typeClass = LocalDateUserType.class),
    @TypeDef(name = "localDateTimeType",
            defaultForType = LocalDateTime.class,
            typeClass = LocalDateTimeUserType.class),
    @TypeDef(name = "localTimeType",
            defaultForType = LocalTime.class,
            typeClass = LocalTimeUserType.class)
})
package fr.flow.collector.converter.hibernateUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.hibernate.annotations.TypeDefs;
import org.hibernate.annotations.TypeDef;
