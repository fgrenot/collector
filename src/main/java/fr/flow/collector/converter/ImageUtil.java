/*
 * Copyright 2015 florian.grenot.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.flow.collector.converter;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;

/**
 *
 * @author florian.grenot
 */
public class ImageUtil {

    public static BufferedImage byteArrayToBufferedImageConverter(byte[] imageInByte) throws IOException {
        BufferedImage image;

        try (InputStream in = new ByteArrayInputStream(imageInByte)) {
            image = ImageIO.read(in);
        }

        return image;
    }

    public static byte[] bufferedImagetoByteArrayConverter(BufferedImage image, String extention) throws IOException {
        byte[] imageInByte;

        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            ImageIO.write(image, extention, baos);
            baos.flush();
            imageInByte = baos.toByteArray();
        }

        return imageInByte;
    }

    private ImageUtil() {
    }

}
