/*
 * Copyright 2015 florian.grenot.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.flow.collector.converter;

import com.moviejukebox.allocine.model.MovieInfos;
import fr.flow.collector.model.Movie;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author florian.grenot
 */
public class MovieConverter {

    public static Map<String, Movie> allocineMoviesListToMovieListConverter(List<com.moviejukebox.allocine.model.Movie> allocineMovies) {
        Map<String, Movie> movies = new HashMap<>(allocineMovies.size());

        for (com.moviejukebox.allocine.model.Movie allocineMovie : allocineMovies) {
            String title = allocineMovie.getTitle() == null ? allocineMovie.getOriginalTitle() : allocineMovie.getTitle();
            String posterURL = allocineMovie.getPoster() == null ? "/Collector/resources/images/empty-movie.png" : allocineMovie.getPoster().getHref();

            Movie movie = new Movie();
            movie.setTitle(title);
            movie.setPosterURL(posterURL);

            movies.put(String.valueOf(allocineMovie.getCode()), movie);
        }

        return movies;
    }

    public static Movie allocineMovieInfosToMovieConverter(MovieInfos allocineMovieInfos) {
        Movie businessMovie = new Movie(allocineMovieInfos.getTitle(),
                integerTimeToDateConverter(allocineMovieInfos.getRuntime()),
                stringDateToDateConverter(allocineMovieInfos.getReleaseDate()),
                allocineMovieInfos.getSynopsis());

        businessMovie.setPosterURL("/Collector/resources/images/empty-movie.png");
        for (String url : allocineMovieInfos.getPosterUrls()) {
            businessMovie.setPosterURL(url);
            break;
        }
        businessMovie.setActors(ActorConverter.allocineMoviePersonSetToActorListConverter(allocineMovieInfos.getActors()));
        businessMovie.setCategories(CategoryConverter.allocineGenreToCategoryListConverter(allocineMovieInfos.getGenres()));

        return businessMovie;
    }

    private static LocalTime integerTimeToDateConverter(int timeInSec) {
        int hour = timeInSec / 3600;
        timeInSec %= 3600;
        int minute = timeInSec / 60;
        timeInSec %= 60;
        return LocalTime.of(hour, minute, timeInSec);
    }

    private static LocalDate stringDateToDateConverter(String date) {
        return LocalDate.parse(date);
    }

    private MovieConverter() {
    }

}
