/*
 * Copyright 2015 florian.grenot.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.flow.collector.converter;

import fr.flow.collector.model.Business;
import java.beans.PropertyEditorSupport;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Florian
 * @param <T>
 */
public class Converter<T extends Business> extends PropertyEditorSupport {

    private final Map<String, T> businesses = new HashMap<>();

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        if (!businesses.containsKey(text)) {
            throw new IllegalArgumentException("The converter doesn't contains '" + text + "' value.");
        }

        setValue(businesses.get(text));
    }

    public boolean isEmpty() {
        return businesses.isEmpty();
    }

    public void addBusiness(String id, T business) {
        businesses.put(id, business);
    }

}
