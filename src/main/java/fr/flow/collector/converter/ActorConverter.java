/*
 * Copyright 2015 florian.grenot.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.flow.collector.converter;

import com.moviejukebox.allocine.model.MoviePerson;
import com.moviejukebox.allocine.model.PersonInfos;
import com.moviejukebox.allocine.model.ShortPerson;
import fr.flow.collector.model.Actor;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author florian.grenot
 */
public class ActorConverter {

    public static List<Actor> allocineMoviePersonSetToActorListConverter(Set<MoviePerson> allocineActors) {
        List<Actor> actors = new ArrayList<>(allocineActors.size());

        for (MoviePerson allocineActor : allocineActors) {
            Actor actor = new Actor(allocineActor.getName(), null);
            actors.add(actor);
        }

        return actors;
    }

    public static Actor allocinePersonInfosToActorConverter(PersonInfos allocineActor) {
        return new Actor(allocineActor.getFirstName(),
                allocineActor.getLastName(),
                birthdayConverter(allocineActor.getBirthDate()));
    }

    public static Map<String, Actor> allocineActorListToActorMapConverter(List<ShortPerson> persons) {
        Map<String, Actor> actors = new HashMap<>(persons.size());

        for (ShortPerson person : persons) {
            Actor actor = new Actor(person.getName(), null);
            actors.put(String.valueOf(person.getCode()), actor);
        }

        return actors;
    }

    private static Date birthdayConverter(String dateInString) {
        if (null == dateInString || dateInString.isEmpty()) {
            return null;
        }
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;

        try {
            date = formatter.parse(dateInString);
        }
        catch (ParseException ex) {
            Logger.getLogger(ActorConverter.class.getName()).log(Level.WARNING, "{0}: return null.", ex.getMessage());
        }

        return date;
    }

    private ActorConverter() {
    }

}
